﻿#define MAX_N 28123
#include <stdio.h>
#include <math.h>
#include <time.h>

int numbersSum() // Функция нахождения суммы положительных чисел, которые не могут быть записаны как сумма двух избыточных чисел
{
	int i = 0, j = 0, redundantNumberCounter = 0, numberCounter = 0; // счетчики 
	int divider, number, flag = 0, sumOfDividers = 0;
	int redundantNumbers[MAX_N]; // массив для полученных избыточных чисел
	long int sumOfNumbers = 0; // искомая сумма
	for (number = 1; number <= MAX_N; number++) // нахождение избыточных чисел в диапазоне 0 - 28123
	{
		for (divider = 1; divider <= MAX_N; divider++)
			if (number % divider == 0 && number != divider)
				sumOfDividers += divider;
		if (number < sumOfDividers)
		{
			redundantNumbers[i] = number;
			redundantNumberCounter++;
			i++;
		}
		sumOfDividers = 0;
	}
	for (number = 1; number <= MAX_N; number++)
	{
		for (j = 0; j <= redundantNumberCounter; j++)
			for (i = 0; i <= redundantNumberCounter; i++)
				if (number == (redundantNumbers[j] + redundantNumbers[i])) // если неподходящее условие выполняется
					flag = 1;
		if (flag == 0) // если число удовлетворяет условию 
			sumOfNumbers += number;
		flag = 0;
	}
	return sumOfNumbers;
}

int main()
{
	clock_t start, finish;
	double t;
	start = clock();
	long int result = numbersSum();
	printf("The sum of suitable numbers is %ld\n", result);
	finish = clock();
	t = (double)(finish - start) / CLOCKS_PER_SEC;
	(t <= 100) ? printf("The program has worked %.1f sec\n", t) : printf("The program has worked %.0f min %.0f sec\n", t / 60, (int)(t) % 60);
	return 0;
}