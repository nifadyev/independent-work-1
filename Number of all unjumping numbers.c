#define N 10000000000
#include <stdio.h>
#include <math.h>
#include <time.h>
typedef unsigned long long int ULLI;

ULLI unjumpingNumberCounter()
{
	ULLI increasingNumberCounter = 0, EqualNumberCounter = 0, decreasingNumberCounter = 0, i; // �������� ��������������� ����� � ����� � ����������� �������
	ULLI number;
	ULLI tmp; // ��������� ���������� ��� number
	int increasingNumberFlag = 0, decreasingNumberFlag = 0, EqualNumberFlag = 0, lengh = 0;
	int leftDigit = 0, rightDigit = 0;
	for (number = 10; number < N; number++) // ����� �� 1 �� 9 ��� ������ ������������
	{
		tmp = number;
		for (i = number; i != 0; i /= 10)
			lengh++;
		for (i = 0; i < lengh; i++)
		{
			leftDigit = (tmp % 100) / 10;
			rightDigit = tmp % 10;
			if (leftDigit <= rightDigit)
				increasingNumberFlag++;
			if (leftDigit >= rightDigit)
				decreasingNumberFlag++;
			if (leftDigit == rightDigit)
				EqualNumberFlag++;
			tmp = tmp / 10;
		}
		if (lengh == increasingNumberFlag)
			increasingNumberCounter++;
		if (lengh == decreasingNumberFlag + 1)
			decreasingNumberCounter++;
		if (lengh == EqualNumberFlag + 1)
			EqualNumberCounter++;
		lengh = increasingNumberFlag = EqualNumberFlag = decreasingNumberFlag = 0;
	}
	return increasingNumberCounter - EqualNumberCounter + decreasingNumberCounter; /* ����� ���� 1111 �� ����������� ��������
�������������, ��� � ����������, ������� �������� ��, �.�. ��� �������������� ������*/
}

int main()
{
	clock_t start, finish;
	start = clock();
	double t;
	ULLI result = unjumpingNumberCounter();
	printf("There are %llu suitable numbers\n", result + 9); // ���������� �����, ���������� ������������ (1-9)
	finish = clock();
	t = (double)(finish - start) / CLOCKS_PER_SEC;
	if (t <= 100)
		printf("The program has worked %.1f sec\n", t);
	else
		printf("The program has worked %.0f min %.0f sec\n", t / 60, (int)(t) % 60);
	return 0;
}